#!/bin/bash
set -e

if [ -z "$1" ]; then
    read -p "What's your gitlab.com username? " username
else
    username=$1
fi

if ssh -T git@gitlab.com >/dev/null; then
    proto=ssh
    extra=git@
else
    proto=https
    extra=$username@
fi

for file in $(cat .subrepos); do
    pushd $file >/dev/null
    if ! (git remote -v | grep -q upstream); then
        git remote rename origin upstream
        git remote add origin $proto://${extra}gitlab.com/$username/$file.git
    fi
    popd >/dev/null
done
